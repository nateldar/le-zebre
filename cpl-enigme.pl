/* X est à droite de Y */
a_droite(X, Y) :- X is Y+1.
/* X est à gauche de Y */
a_gauche(X, Y) :- a_droite(Y, X).

/* X et Y sont voisins */
voisin(X, Y) :- a_droite(X, Y).
voisin(X, Y) :- a_gauche(X, Y).

/* Maisons est solution du problème du zèbre */
solution(Maisons) :-
    Maisons = [
           maison(1, Nationalite1,  Couleur1,     Animal1,   Boisson1,    Cigarette1),
           maison(2, Nationalite2,  Couleur2,     Animal2,   Boisson2,    Cigarette2),
           maison(3, Nationalite3,  Couleur3,     Animal3,   Boisson3,    Cigarette3),
           maison(4, Nationalite4,  Couleur4,     Animal4,   Boisson4,    Cigarette4),
           maison(5, Nationalite5,  Couleur5,     Animal5,   Boisson5,    Cigarette5)],
    member(maison(_, anglais,       rouge,        _,         _,           _           ), Maisons),
    member(maison(_, espagnol,      _,            chien,     _,           _           ), Maisons),
    member(maison(A, _,             vert,         _,         cafe,        _           ), Maisons),
    member(maison(_, ukrainien,     _,            _,         the,         _           ), Maisons),
    member(maison(B, _,             blanc,        _,         _,           _           ), Maisons),
    a_droite(A, B),
    member(maison(_, _,             _,            escargot,  _,           old_gold    ), Maisons),
    member(maison(_, _,             jaune,        _,         _,           kool        ), Maisons),
    member(maison(3, _,             _,            _,         lait,        _           ), Maisons),
    member(maison(1, norvegien,     _,            _,         _,           _           ), Maisons),
    member(maison(C, _,             _,            _,         _,           chesterfield), Maisons),
    member(maison(D, _,             _,            renard,    _,           _           ), Maisons),
    voisin(C, D),
    member(maison(E, _,             _,            cheval,    _,           _           ), Maisons),
    member(maison(F, _,             _,            _,         _,           kool        ), Maisons),
    voisin(E, F),
    member(maison(_, _,             _,            _,         vin,         gitane      ), Maisons),
    member(maison(_, japonais,      _,            _,         _,           craven      ), Maisons),
    member(maison(G, norvegien,     _,            _,         _,           _           ), Maisons),
    member(maison(H, _,             bleu,         _,         _,           _           ), Maisons),
    voisin(G, H),
    member(maison(_, _,             _,            zebre,     _,           _           ), Maisons),
    member(maison(_, _,             _,            _,         eau,         _           ), Maisons).

/* Habitant est le propriétaire de Animal */
proprietaire(Habitant, Animal) :- solution(Maisons), member(maison(_, Habitant, _, Animal, _, _), Maisons).
/* Habitant est buveur de Boisson */
buveur(Habitant, Boisson) :- solution(Maisons), member(maison(_, Habitant, _, _, Boisson, _), Maisons).
