# Le Zèbre

Résolution de l'énigme du zèbre (ou énigme d'Einstein) via la programmation logique et la programmation par contraintes (swi-prolog) : https://fr.wikipedia.org/wiki/%C3%89nigme_d%27Einstein

Voici l'énoncé que nous avons utilisé :  

Dans une rue, il y a 5 maisons de couleurs différentes, chacune d'elle est habitée par une personne de nationalité différente, qui possède un animal différent, boit une boisson différente et fume une marque de cigarette différente. Une liste de 14 faits est connue :

* l'anglais habite la maison rouge
* le chien appartient à l'espagnol
* on boit du café dans la maison verte
* l'ukrainien boit du thé
* la maison verte est à côté de la maison blanche à droite
* le fumeur de old gold élève des escargots
* on fume des kool dans la maison jaune
* on boit du lait dans la maison du milieu
* le norvégien habite la première maison à gauche
* le fumeur de chesterfield habite à côté du propriétaire du renard
* le fumeur de kool habite à côté du propriétaire du cheval
* le fumeur de gitanes boit du vin
* le japonais fume des craven
* le norvégien habite à côté de la maison bleue.

A partir de ces données, il faut répondre à deux questions :

* Qui est le propriétaire du zèbre ?
* Qui boit de l'eau ?



