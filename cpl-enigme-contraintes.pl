:- use_module(library(clpfd)).

zebre(ProprietaireZebre,BuveurEau) :-
  Nationalite = [Anglais, Japonais, Espagnol, Ukrainien, Norvegien],
  Couleur = [Rouge, Vert, Blanc, Jaune, Bleu],
  Cigarette = [Old_gold, Kool, Chesterfield, Gitane, Craven],
  Animal = [Chien, Escargot, Renard, Cheval, Zebre],
  Boisson = [Cafe, The, Lait, Vin, Eau],

  Nationalite ins 1..5,
  Couleur  ins 1..5,
  Cigarette  ins 1..5,
  Animal    ins 1..5,
  Boisson  ins 1..5,

  all_different(Nationalite),
  all_different(Couleur),
  all_different(Cigarette),
  all_different(Animal),
  all_different(Boisson),

  Anglais #= Rouge,
  Chien #= Espagnol,
  Cafe #= Vert,
  Ukrainien #= The,
  Vert #= Blanc + 1,
  Old_gold #= Escargot,
  Kool #= Jaune,
  Lait #= 3,
  Norvegien #= 1,
  (Chesterfield #= Renard + 1 #\/ Chesterfield #= Renard - 1 ),
  (Kool #= Cheval - 1 #\/ Kool #= Cheval + 1),
  Gitane #= Vin,
  Japonais #= Craven,
  (Norvegien #= Bleu  - 1 #\/ Norvegien #= Bleu  + 1),

  flatten([Nationalite, Couleur, Cigarette, Animal, Boisson], Liste), label(Liste),

  NomNationalite = [Anglais-anglais, Espagnol-espagnol, Japonais-japonais,
            Ukrainien-ukrainien, Norvegien-norvegien],
  memberchk(Zebre-ProprietaireZebre, NomNationalite),
  memberchk(Eau-BuveurEau, NomNationalite).
